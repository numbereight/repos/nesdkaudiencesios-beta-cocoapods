## Build instructions
This project is built with Xcode.

[![pipeline status](https://gitlab.com/numbereight/repos/nesdkaudiencesios-beta-cocoapods/badges/master/pipeline.svg)](https://gitlab.com/numbereight/repos/nesdkaudiencesios-beta-cocoapods/-/commits/master)

## Maintainers

* [Matthew Paletta](matt@numbereight.ai)
* [Chris Watts](chris@numbereight.ai)
