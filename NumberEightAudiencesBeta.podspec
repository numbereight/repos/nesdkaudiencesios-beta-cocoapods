Pod::Spec.new do |spec|

  spec.platform          = :ios
  spec.ios.deployment_target = '12.0'

  spec.name              = 'NumberEightAudiencesBeta'
  spec.module_name       = 'Audiences'
  spec.header_dir        = spec.module_name
  spec.version = '3.11.7'
  spec.summary           = '--'

  spec.homepage          = 'https://www.numbereight.ai/'
  spec.documentation_url = 'http://docs.numbereight.ai/'
  spec.license           =  {
    :type => 'MIT',
    :file => 'LICENSE.md'
  }
  spec.authors           = {
    'Matthew Paletta' => 'matt@numbereight.ai',
    'Chris Watts' => 'chris@numbereight.ai'
  }

  spec.source = {
    :git => 'https://gitlab.com/numbereight/repos/nesdkaudiencesios-beta-cocoapods.git',
    :tag => spec.version
  }

  spec.cocoapods_version = '>= 1.8.0'
  spec.swift_versions = [ '5.0' ]

  spec.libraries = "c++"

  spec.dependency 'NumberEightInsightsBeta', spec.version.to_s
  spec.source_files = 'Source/**/*.{h,swift,m,mm,c,cc,cpp}'
  spec.public_header_files = 'Source/**/*.h'
  spec.requires_arc = true
  spec.static_framework = true
end
