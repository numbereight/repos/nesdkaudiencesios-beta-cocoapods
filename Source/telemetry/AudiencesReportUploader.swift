//
//  AudiencesReportUploader.swift
//  Audiences
//
//  Created by Matthew Paletta on 2021-11-22.
//  Copyright © 2021 NumberEight Technologies Ltd. All rights reserved.
//

import Foundation
import Insights
import NumberEightCompiled

internal struct AudienceUploadBody: Encodable {
    // Do not changes these names, they affect the server encoding.
    var app_id: String
    var data: AudiencesUsageReport
}

enum UploadError: Error {
    // Occurs when we were unable to generate a URL Request to the NumberEight server.
    case failedToGetURLRequest
    case serverError
    // Occurs when the data uploaded is in a bad format
    case unacceptable
    case noData
}

class AudiencesReportUploader {
    private static let LOG_TAG = "AudiencesReportUploader"

    private static func makeWebRequest(path: String, method: NEHTTPMethod, data: String) -> NEXHTTPRequest? {
        let req = NEXHTTPRequest.newNERequest(with: method)
        req.path = path
        req.data = data
        req.setHeaderWithKey("Content-Type", value: "application/json")
        return req
    }

    static func makeAudiencesRequest(data: String) -> NEXHTTPRequest? {
        return self.makeWebRequest(path: "logs/audiences", method: .POST, data: data)
    }

    static func encodeUploadBody(appId: String, report: AudiencesUsageReport) throws -> Data {
        let uploadBody = AudienceUploadBody(app_id: appId,
                                            data: report)

        let encoder = JSONEncoder()
        return try encoder.encode(uploadBody)
    }

    func uploadReport(report: AudiencesUsageReport, callback: @escaping (Error?) -> Void) {
        let appId = Bundle.main.bundleIdentifier ?? "NOBUNDLEID"

        var dataOrNil: Data?
        do {
            dataOrNil = try AudiencesReportUploader.encodeUploadBody(appId: appId, report: report)
        } catch let error {
            callback(error)
            return
        }

        guard let data = dataOrNil, let dataStr = String(data: data, encoding: .utf8) else {
            callback(UploadError.noData)
            return
        }

        guard let req = AudiencesReportUploader.makeAudiencesRequest(data: dataStr) else {
            callback(UploadError.failedToGetURLRequest)
            return
        }

        // NEXHTTPService.httpRequest(req) { response in
        //     if response.ok() {
        //         callback(nil)
        //     } else if response.unacceptable() {
        //         NELog.msg(AudiencesReportUploader.LOG_TAG, error: "The server permanently rejected the usage counts.", metadata: ["status_code": response.statusCode, "message": response.data])
        //         callback(UploadError.unacceptable)
        //     } else {
        //         NELog.msg(AudiencesReportUploader.LOG_TAG, warning: "Error while uploading usage counts.", metadata: ["status_code": response.statusCode, "message": response.data])
        //         callback(UploadError.serverError)
        //     }
        // }

        // handle 'unused' variable warning on req.
        if (dataStr == dataStr + "0") {
            print(req)
        }

        // Don't actually send the request anymore (will be added in the future)
        // instead, just say it passed.
        callback(nil)
    }
}
