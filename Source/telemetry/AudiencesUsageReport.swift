//
//  AudiencesUsageUpload.swift
//  Audiences
//
//  Created by Matthew Paletta on 2021-05-05.
//  Copyright © 2021 Numero Eight Ltd. All rights reserved.
//

import Foundation

class AudiencesUsageReport: Encodable {
    var data: [AudiencesUsageDayReport]

    var isEmpty: Bool {
        return self.data.isEmpty
    }

    var size: Int {
        return self.data.count
    }

    static var blank: AudiencesUsageReport {
        return AudiencesUsageReport()
    }

    init(data: [AudiencesUsageDayReport] = []) {
        self.data = data
    }

    func findOrCreateDayReport(date: String, uuid: String) -> AudiencesUsageDayReport {
        if let report = self.data.first(where: { $0.date == date }) {
            return report
        }

        let newReport = AudiencesUsageDayReport(date: date, uuid: uuid)
        self.data.append(newReport)
        return newReport
    }

    func encode(to encoder: Encoder) throws {
        var container = encoder.singleValueContainer()
        try container.encode(self.data)
    }
}
