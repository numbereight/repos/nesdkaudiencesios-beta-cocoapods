//
//  Audiences.swift
//  Audiences
//
//  Created by Chris Watts on 12/08/2020.
//  Copyright © 2020 NumberEight Technologies Ltd. All rights reserved.
//

import Foundation
import Insights
import NumberEightCompiled
import Dispatch

// MARK: -

// NumberEight Audiences implementation
public class AudiencesInternal: NSObject {
    static let instance = AudiencesInternal()

    private override init() {
        // This defaults is shared with AudiencesReporter, beware of overwriting keys.
        // swiftlint:disable force_unwrapping
        self.defaults = UserDefaults(suiteName: "ai.numbereight.audiences")!
        // swiftlint:enable force_unwrapping

        super.init()
        NumberEight.add(dataProvider: self)
    }

    internal static var isRunning: Bool {
        return AudiencesInternal.instance._isRunning
    }

    internal var _isRunning: Bool = false

    @objc
    public static let defaultParameters = LiveAudiences.defaultParameters

    @objc
    public var shouldPrintErrors = true

    private var isRegisteredAsConsentChangeListener = false

    internal static let LOG_TAG = "AudiencesInternal"
    internal let defaults: UserDefaults

    internal var apiToken: APIToken?
    internal var audienceReporter: AudiencesReporter?
    internal var liveAudiences: LiveAudiences?

    @objc
    public static var shouldPrintErrors: Bool {
        get {
            return AudiencesInternal.instance.shouldPrintErrors
        }
        set(newValue) {
            AudiencesInternal.instance.shouldPrintErrors = newValue
        }
    }

    @objc
    public static var currentMemberships: NSSet {
        let memberships = AudiencesInternal.instance.fetchMemberships()
        _ = AudiencesInternal.instance.audienceReporter?.markAudiencesFetched(memberships: memberships)
        return NSSet(set: memberships)
    }

    @objc
    public static func startRecording(apiToken: APIToken, parameters: [String: Parameters] = AudiencesInternal.defaultParameters) {
        AudiencesInternal.instance._startRecording(apiToken: apiToken, parameters: parameters)
    }

    @objc
    public static func startRecording(apiToken: APIToken, parameters: [String: Parameters] = AudiencesInternal.defaultParameters, onStart: @escaping (_ error: Error?) -> Void) {
        AudiencesInternal.instance._startRecording(apiToken: apiToken, parameters: parameters) { result in
            switch result {
            case .success:
                onStart(nil)
            case let .failure(error):
                onStart(error)
            }
        }
    }

    @objc
    public static func updateTopics(parameters: [String: Parameters]) {
        AudiencesInternal.instance.liveAudiences?.updateTopics(parameters: parameters)
    }

    func _startRecording(apiToken: APIToken, parameters: [String: Parameters], onStart: ((Result<Void, Error>) -> Void)? = nil) {
        let callback = onStart ?? { (result: Result<Void, Error>) -> Void in
            switch result {
            case .success:
                NELog.msg(AudiencesInternal.LOG_TAG, info: "NumberEight Audiences started successfully")
            case let .failure(error):
                NELog.msg(AudiencesInternal.LOG_TAG, error: "NumberEight Audiences failed to start.", metadata: ["error": error.localizedDescription])
            }
        }

        // Try and restore the original AudienceReporter & LiveAudiences, otherwise create a new one.
        self.apiToken = apiToken
        let audienceReporter = self.audienceReporter ?? AudiencesReporter()
        let liveAudiences = self.liveAudiences ?? LiveAudiences()

        // We have to register first in case we don't have consent at the beginning.
        if !self.isRegisteredAsConsentChangeListener {
            NumberEight.add(onConsentChangeListener: self)
            self.isRegisteredAsConsentChangeListener = true
        }

        // Prepare Insights config
        let id = self.getAudiencesId()
        let config = { () -> RecordingConfig in
            if Insights.config == nil {
                // If no config present, remove the weather from
                // the list of defaults
                let config = RecordingConfig.default
                config.uploadWithWifiOnly = false
                return config
            } else {
                // Fallthrough to the existing config
                return Insights.config ?? RecordingConfig.default
            }
        }()

        // Only store the config in Insights, so that when we do get consent, Insights will start.
        // This does not start any processing.
        if config.deviceId != id {
            Insights.stopRecording()

            config.deviceId = id
            config.uploadWithWifiOnly = false
            InsightsInternal.ext._initializeRecording(apiToken: apiToken, config: config)
        }

        // If we have consent, start recording.
        if NumberEight.consentOptions().hasRequiredConsent(self) {
            self._isRunning = true
            // Don't pass in the user callback otherwise it may be called multiple times.
            Insights.startRecording(apiToken: apiToken, config: config)

            // Fetch the current memberships without marking it as a count.
            let memberships = self.fetchMemberships()
            let didInitCounts = audienceReporter.initialise(memberships: memberships)
            if !didInitCounts {
                NELog.msg(AudiencesInternal.LOG_TAG, warning: "Could not set up audience reporter, see errors above.")
            }

            liveAudiences.start(parameters: parameters)
            self.audienceReporter = audienceReporter
            self.liveAudiences = liveAudiences

            callback(.success(()))
        } else {
            callback(.failure(NSError(domain: "NumberEight Audiences does not have the required consent required to run.", code: 1, userInfo: nil)))
        }
    }

    @objc
    public static func pauseRecording() {
        AudiencesInternal.instance._pauseRecording()
    }

    func _pauseRecording() {
        self._isRunning = false
        Insights.pauseRecording()

        self.audienceReporter?.stop()
        self.liveAudiences?.stop()
    }

    @objc
    public static func stopRecording() {
        AudiencesInternal.instance._stopRecording()
    }

    func _stopRecording() {
        self._isRunning = false
        NumberEight.remove(onConsentChangeListener: self)
        self.isRegisteredAsConsentChangeListener = false
        Insights.stopRecording()

        self.audienceReporter?.stop()
        self.liveAudiences?.stop()
    }

    // MARK: - Internal helper funtions -
    private func fetchMemberships() -> Set<Membership> {
        let habitualAudiences = InsightsInternal.ext.audienceMemberships

        // If for whatever reason we can't get the live audiences, return the
        // habitual once, better than having no audiences.
        let liveContextAudiences = self.liveAudiences?.retrieveAudiences() ?? Set<Membership>()

        // Merge audiences together.
        let historicalAudienceIds = liveContextAudiences.map { $0.id }
        var mergedAudiences = liveContextAudiences
        for habitual in habitualAudiences where !historicalAudienceIds.contains(habitual.id) {
            // Add a habitual audience where there is not already a historical audience
            // with that same ID.
            // Can't do a direct comparison, because the liveness will be different, so the
            // set will accept both values.
            mergedAudiences.insert(habitual)
        }

        return mergedAudiences
    }

    private static func generateAudiencesId() -> String {
        return NumberEight.deviceID()
    }

    private func getAudiencesId() -> String {
        if let insightsId = Insights.config?.deviceId {
            return insightsId
        }

        let audiencesUserDefaultsKey = "audiencesId"
        let id = self.defaults.string(forKey: audiencesUserDefaultsKey) ?? { () in
            let newID = AudiencesInternal.generateAudiencesId()
            self.defaults.set(newID, forKey: audiencesUserDefaultsKey)
            return newID
        }()

        return id
    }
}

// MARK: - Helper extensions -
public extension NumberEight {
    class var `Audiences`: Audiences.Type {
        typealias NEAudiences = Audiences
        return NEAudiences.self
    }
}
