//
//  Audiences.h
//  Audiences
//
//  Created by Chris Watts on 12/08/2020.
//  Copyright © 2020 NumberEight Technologies Ltd. All rights reserved.
//

#import <Foundation/Foundation.h>

//! Project version number for Audiences.
FOUNDATION_EXPORT double AudiencesVersionNumber;

//! Project version string for Audiences.
FOUNDATION_EXPORT const unsigned char AudiencesVersionString[];

#import <Audiences/NEXAudiences.h>

// Need to import this so Insights automatically gets included in
// every Swift file. This seems to affect some Xcode projects.
// Not sure why it only affects some, but not others.
#ifdef SWIFT_TYPEDEFS
@import Insights;
#else
// We want to import it without using the @import for projects that aren't using modules,
// and not using Swift, such as Unity.
#import <Insights/Insights.h>
#endif
