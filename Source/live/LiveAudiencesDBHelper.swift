//
//  LiveAudiencesDBHelper.swift
//  Audiences
//
//  Created by Matthew Paletta on 2021-11-25.
//  Copyright © 2021 NumberEight Technologies Ltd. All rights reserved.
//

import Foundation
import SQLite

struct LiveAudiencesDBHelper {
    private static let version: Int32 = 1
    private static let name = "audiences.liveness.db"
    private static let LOG_TAG = "LiveAudiencesDBHelper"

    public static let AUDIENCE_OBSERVATIONS_TABLE = "observations"

    static var connection: Connection? = {
        guard let loc = LiveAudiencesDBHelper.location else {
            return nil
        }
        let result =
        Swift.Result { () -> Connection in
            try Connection(loc)
        }
        switch result {
        case let .success(conn):
            if LiveAudiencesDBHelper.version != conn.userVersion {
                NELog.msg(LiveAudiencesDBHelper.LOG_TAG,
                          info: "SQLite db version changed.", metadata: ["old": conn.userVersion ?? 1, "new": LiveAudiencesDBHelper.version])
                conn.onVersionChange(newVersion: LiveAudiencesDBHelper.version)
            }
            conn.onCreate()
            return conn
        case let .failure(error):
            NELog.msg(LiveAudiencesDBHelper.LOG_TAG, error: "Failed to get connection", metadata: ["error": error])
            return nil
        }
    }()

    private static var location: Connection.Location? = {
        let result = Swift.Result { () -> String in
            try FileManager
                .default
                .url(for: .applicationSupportDirectory,
                        in: .userDomainMask,
                        appropriateFor: nil,
                        create: true)
                .appendingPathComponent(LiveAudiencesDBHelper.name)
                .absoluteString
        }
        switch result {
        case let .success(urlStr):
            return .uri(urlStr)
        case let .failure(error):
            NELog.msg(LiveAudiencesDBHelper.LOG_TAG, error: "Failed to get connection location.", metadata: ["error": error])
            return nil
        }
    }()

    ///
    /// Creates the SQLite table schema necessary for Audience Liveness.
    ///
    /// - Note: This will only create the table if they don't already exist.  This is safe to call multiple times.
    ///
    /// - Returns: Boolean indicating if it succesfully created the tables (`observations`)
    ///
    /// - Tag: LiveAudiencesDBHelper.createTable
    /// 
    internal func createTable() -> Bool {
        guard let db = LiveAudiencesDBHelper.connection else {
            NELog.msg(LiveAudiencesDBHelper.LOG_TAG, error: "Failed to acquire database connection.")
            return false
        }

        return LiveAudiencesDBHelper.createTable(connection: db)
    }

    internal static func createTable(connection db: Connection) -> Bool {
        do {
            try db.transaction(.exclusive) {
                let audienceObservationsStmt = try db.prepare("""
                CREATE TABLE IF NOT EXISTS \(LiveAudiencesDBHelper.AUDIENCE_OBSERVATIONS_TABLE) (
                    audience_id TEXT,
                    last_observation TEXT NOT NULL,
                    PRIMARY KEY (audience_id)
                )
                """)

                try audienceObservationsStmt.run()
            }

#if DEBUG
            db.trace {
                NELog.msg(LiveAudiencesDBHelper.LOG_TAG, verbose: "DB Trace: \($0)")
            }
#endif
            return true
        } catch let Result.error(message, code, statement) {
            NELog.msg(LiveAudiencesDBHelper.LOG_TAG, error: "An SQLite Error occured while trying to create a table.",
                      metadata: ["code": code, "message": message, "statement": statement.debugDescription])
            return false
        } catch let error {
            NELog.msg(LiveAudiencesDBHelper.LOG_TAG, error: "An error occured while creating table.", metadata: ["error": error.localizedDescription])
            return false
        }
    }

    private func dropTable() -> Bool {
        guard let db = LiveAudiencesDBHelper.connection else {
            NELog.msg(LiveAudiencesDBHelper.LOG_TAG, error: "Failed to acquire database connection while dropping table.")
            return false
        }

        return LiveAudiencesDBHelper.dropTable(connection: db)
    }

    internal static func dropTable(connection db: Connection) -> Bool {
        do {
            try db.transaction(.exclusive) {
                let audienceObservationsStmt = try db.prepare("DROP TABLE IF EXISTS \(LiveAudiencesDBHelper.AUDIENCE_OBSERVATIONS_TABLE)")
                try audienceObservationsStmt.run()
            }

#if DEBUG
            db.trace {
                NELog.msg(LiveAudiencesDBHelper.LOG_TAG, verbose: "DB Trace: \($0)")
            }
#endif
            return true
        } catch let Result.error(message, code, statement) {
            NELog.msg(LiveAudiencesDBHelper.LOG_TAG, error: "An SQLite Error occured while dropping table.", metadata: ["code": code, "message": message, "statement": statement.debugDescription])
            return false
        } catch let error {
            NELog.msg(LiveAudiencesDBHelper.LOG_TAG, error: "An error occured while dropping table.", metadata: ["error": error.localizedDescription])
            return false
        }
    }

    internal func clearAllData() -> Bool {
        guard let db = LiveAudiencesDBHelper.connection else {
            NELog.msg(LiveAudiencesDBHelper.LOG_TAG, error: "Failed to acquire database connection.")
            return false
        }

        do {
            try db.transaction(.exclusive) {
                let audienceReportingStmt = try db.prepare("DELETE FROM \(LiveAudiencesDBHelper.AUDIENCE_OBSERVATIONS_TABLE)")
                try audienceReportingStmt.run()
            }

#if DEBUG
            db.trace {
                NELog.msg(LiveAudiencesDBHelper.LOG_TAG, verbose: "DB Trace: \($0)")
            }
#endif
            return true
        } catch let Result.error(message, code, statement) {
            NELog.msg(LiveAudiencesDBHelper.LOG_TAG, error: "An SQLite error occured while clearing all data.", metadata: ["code": code, "message": message, "statement": statement.debugDescription])
            return false
        } catch let error {
            NELog.msg(LiveAudiencesDBHelper.LOG_TAG, error: "An error occured while dropping data in table.", metadata: ["error": error.localizedDescription])
            return false
        }
    }

}

fileprivate extension Connection {
    private static let LOG_TAG = "Connection"

    private(set) var userVersion: Int32? {
        get {
            let result = Swift.Result { () -> Int32? in
                guard let v = try scalar("PRAGMA user_version") as? Int64 else {
                    return nil
                }
                return Int32(v)
            }
            switch result {
            case let .success(uv):
                return uv
            case let .failure(error):
                NELog.msg(Connection.LOG_TAG, error: "An exception occured while fetching connection", metadata: ["error": error])
                return nil
            }
        }
        set {
            guard let nv = newValue else {
                return
            }
            do {
                try run("PRAGMA user_version = \(nv)")
            } catch {
                NELog.msg(Connection.LOG_TAG, error: "An exception occured while fetching connection", metadata: ["error": error])
            }
        }
    }

    func onCreate() {
        _ = LiveAudiencesDBHelper.createTable(connection: self)
    }

    func onVersionChange(newVersion: Int32) {
        _ = LiveAudiencesDBHelper.dropTable(connection: self)
        _ = LiveAudiencesDBHelper.createTable(connection: self)

        self.userVersion = newVersion
    }
}
